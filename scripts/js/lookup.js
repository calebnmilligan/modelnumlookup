onload_queried = false;

function populateTable(data) {
	let error_container = document.getElementById("error-container");
	let table_container = document.getElementById("table-container");
	if (data.items.length === 0) {
		showErrors(["No results"]);
		removeClass(error_container, "hidden");
		if (!table_container.classList.contains("selected")) {
			addClass(table_container, "hidden");
		}
	}
	else {
		removeClass(table_container, "hidden");
		if (!error_container.classList.contains("selected")) {
			addClass(error_container, "hidden");
		}
	}
	const table_body = document.querySelector("#table-results > tbody");
	table_body.innerHTML = "";
	data.items.forEach(function (row) {
		const table_row = document.createElement("tr");
		let mn_null = row["model_number"] == null ? " class=\"gray\"" : "";
		let make_null = row["make"] == null ? " class=\"gray\"" : "";
		table_row.innerHTML = `<td${mn_null}><a href="${getRoot()}/details/?id=${row["id"]}">${row["model_number"]}</a></td><td${make_null}>${row["make"]}</td><td>`
			+ `${row["model"]}</td><td>${row["type"]}</td><td>$${row["condition_excellent"]}</td><td>`
			+ `$${row["condition_very_good"]}</td><td>$${row["condition_good"]}</td><td>`
			+ `$${row["condition_fair"]}</td><td>$${row["condition_poor_untested"]}</td><td>`
			+ `$${row["condition_nonfunctional"]}</td>`;
		table_row.onclick = toggleSelected;
		table_body.appendChild(table_row);
	});
}

function toggleSelected(event) {
	const table_body = document.querySelector("#table-results > tbody");
	let row_elem = event.target.closest("tr");
	let id_elem = row_elem.querySelector(".rowid");
	let rowid = id_elem === undefined ? null : id_elem.innerText;
	if (isNaN(rowid) || !Number.isInteger(rowid = ~~rowid)) {
		return;
	}
	let should_add = !row_elem.classList.contains("selected");
	Array.from(table_body.children).forEach(function (child) {
		removeClass(child, "selected");
	});
	if (should_add) {
		addClass(row_elem, "selected");
	}
}

function submitQuery(push_to_history = false) {
	let field = document.getElementById("input-query-field").value;
	let query = document.getElementById("input-query-text").value;
	if (push_to_history) {
		window.history.pushState({
			field: field,
			query: query
		}, null, `${getRoot()}/?field=${encodeURIComponent(field)}&query=${encodeURIComponent(query)}`);
		populateNavLinks();
	}
	fetchModels(field, query, populateTable);
}

function fetchModels(field, query, callback) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				callback(response.data);
			}
			else {
				let messages = ["There was a problem processing your request"];
				if (response.error) {
					messages.push(response.error.message);
				}
				showErrors(messages);
				document.getElementById("table-container").style.display = "none";
				document.getElementById("error-container").style.display = "block";
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/query.php?field=${encodeURIComponent(field)}&query=${encodeURIComponent(query)}`, true);
	request.send();
}

function disableButtonNewEntry() {
	let elem = document.getElementById("nav-new-entry");
	addClass(elem, "hidden");
}

window.onpopstate = function (event) {
	let state = event.state;
	if (state == null) {
		return;
	}
	let input_field = document.getElementById("input-query-field");
	let input_query = document.getElementById("input-query-text");
	input_field.value = state.field || "";
	input_query.value = state.query || "";
	onload_queried = true;
	fetchModels(state.field, state.query, populateTable);
};

document.addEventListener("DOMContentLoaded", function () {
	document.getElementById("form-query").onsubmit = function () {
		submitQuery(true);
		return false;
	};
	document.getElementById("nav-new-entry").href = getRoot() + "/new/";
	let field = getUrlParameter(null, "field", null);
	let query = getUrlParameter(null, "query", null);
	if (field !== null) {
		document.getElementById("input-query-field").value = (field = decodeURIComponent(field));
	}
	if (query !== null) {
		document.getElementById("input-query-text").value = (query = decodeURIComponent(query));
	}
	if (!onload_queried) {
		onload_queried = true;
		submitQuery();
	}
	getActiveSessionInfo(function (session_info) {
		checkPermission(session_info.user, "entry.create", function (value) {
			if (!value) {
				disableButtonNewEntry();
			}
		});
	});
});
