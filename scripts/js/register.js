function submitRegister() {
	if (!validatePasswordsMatch()) {
		return;
	}
	let request = new XMLHttpRequest();
	let data = new FormData();
	data.append("uname", document.getElementById("input-username").value);
	data.append("email", document.getElementById("input-email").value);
	data.append("passwd", document.getElementById("input-password").value);
	
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				window.location = `${getRoot()}/user/login/`;
			}
		}
	};
	request.open("POST", `${getRoot()}/scripts/php/usermgmt.php`, true);
	request.send(data);
}

function validatePasswordsMatch() {
	let password = document.getElementById("input-password").value;
	let password_confirm = document.getElementById("input-password-confirm").value;
	let valid_indicator = document.getElementById("password-repeated");
	let valid = password === password_confirm && password.trim().length > 0;
	setValidIndicator(valid_indicator, valid);
	valid_indicator.title = valid ? "" : "The passwords do not match";
	return valid;
}

function validatePasswordLength() {
	getMinPasswordLength(function (min_length) {
		let password_length = document.getElementById("input-password").value.length;
		let valid_indicator = document.getElementById("password-valid");
		let valid = password_length >= min_length;
		setValidIndicator(valid_indicator, valid);
		valid_indicator.title = valid ? "" : `Password too short (minimum ${min_length} characters)`;
	});
}

function setValidIndicator(valid_indicator, valid) {
	if (valid) {
		removeClass(valid_indicator, "invalid");
		addClass(valid_indicator, "valid");
		valid_indicator.innerText = "✔";
	}
	else {
		removeClass(valid_indicator, "valid");
		addClass(valid_indicator, "invalid");
		valid_indicator.innerText = "✘";
	}
}

function validateEmail() {
	let valid_indicator = document.getElementById("email-valid");
	let input = document.getElementById("input-email");
	let current_email = input.value.trim();
	input.value = current_email;
	let last_at = current_email.lastIndexOf("@");
	let parts = [current_email.substring(0, last_at), current_email.substring(last_at + 1, current_email.length)];
	if (parts[0].length === 0 || parts[1].length === 0) {
		setValidIndicator(valid_indicator, false);
		valid_indicator.title = "Invalid email address";
		return;
	}
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				setValidIndicator(valid_indicator, !response.exists);
				valid_indicator.title = response.exists ? "This email is in use" : "";
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/usermgmt.php?email=${encodeURIComponent(current_email)}`, true);
	request.send();
}

function validateUsername() {
	let valid_indicator = document.getElementById("username-valid");
	let input = document.getElementById("input-username");
	let current_name = input.value.trimStart();
	input.value = current_name;
	current_name = current_name.trim();
	if (current_name.length === 0) {
		setValidIndicator(valid_indicator, false);
		valid_indicator.title = "You must enter a username";
		return;
	}
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				setValidIndicator(valid_indicator, !response.exists);
				valid_indicator.title = response.exists ? "This username is in use" : "";
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/usermgmt.php?uname=${encodeURIComponent(current_name)}`, true);
	request.send();
}

function getMinPasswordLength(callback) {
	/*
	let stored = getCookie("password_length_min");
	if (stored !== null && !isNaN(stored)) {
		callback(~~stored);
		return;
	}
	*/
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				//setCookie("password_length_min", response.data.value, 1);
				callback(response.data.value);
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/getenv.php?key=password_length_min`, true);
	request.send();
}

document.addEventListener("DOMContentLoaded", function () {
	getMinPasswordLength(function (min_length) {
		document.getElementById("min-passwd-len").innerText = min_length;
	});
	validatePasswordLength();
	validatePasswordsMatch();
	validateUsername();
	validateEmail();
	let validator = function () {
		validatePasswordLength();
		validatePasswordsMatch();
	};
	document.getElementById("input-password").addEventListener("keyup", validator);
	document.getElementById("input-password-confirm").addEventListener("keyup", validator);
	document.getElementById("input-username").addEventListener("keyup", validateUsername);
	document.getElementById("input-email").addEventListener("keyup", validateEmail);
});
