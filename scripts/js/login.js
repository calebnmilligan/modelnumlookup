function submitLogin() {
	try {
		let username = document.getElementById("input-username").value;
		let password = document.getElementById("input-password").value;
		login(username, password, loginCallback);
	}
	catch (e) {
		console.error(e);
	}
}

function loginCallback(response) {
	if (response.status === 200) {
		let target = document.getElementById("referrer").value || getRoot();
		console.log(target);
		location.href = target;
	}
}

document.addEventListener("DOMContentLoaded", function () {
	document.getElementById("form-login").onsubmit = function () {
		submitLogin();
		return false;
	};
});
