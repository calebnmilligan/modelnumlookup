function getHrefNoParams() {
	let url = window.location.href;
	if (url.indexOf("?") > 0) {
		url = url.substring(0, url.indexOf("?"));
	}
	if (url.endsWith("/")) {
		url = url.substring(0, url.length - 1);
	}
	return url;
}

function getRoot() {
	let parts = getHrefNoParams().split("//", 2);
	return parts[0] + "//" + parts[1].split("/", 2)[0];
}

function getFirstChildOfType(node, type) {
	let children = node.childNodes;
	if (children == null) {
		return null;
	}
	children = Array.from(children);
	for (let child of children) {
		if (child.nodeType === Node.ELEMENT_NODE && child.tagName.toLowerCase() === type) {
			return child;
		}
	}
	return null;
}

function getUrlParameter(url, key, default_value) {
	url = url || window.location.href;
	let split = url.split("?");
	if (split.length < 2) {
		return default_value;
	}
	let params = split[1].split("&");
	for (const param of params) {
		split = param.split("=");
		if (split[0] === key) {
			return split.length < 2 ? true : split[1];
		}
	}
	return default_value;
}

function removeClass(element, target_class) {
	let current_classes = element.className;
	if (current_classes === undefined || current_classes === null) {
		return;
	}
	let regex = new RegExp(`\\b${target_class}\\b`, "g");
	element.className = current_classes.replace(regex, "");
}

function addClass(element, target_class) {
	element.className = (element.className + " " + target_class).trim();
}

function setCookie(key, value, expire_minutes) {
	let expire = new Date();
	if (expire_minutes === 0) {
		expire_minutes = 365 * 24 * 60;
	}
	expire.setTime(expire.getTime() + (expire_minutes * 60 * 1000));
	document.cookie = `${key}=${value};expires=${expire.toUTCString()};path=/`;
}

function getCookie(key) {
	let name = key + "=";
	let decoded = decodeURIComponent(document.cookie);
	let cookies = decoded.split(";");
	for (let i = 0; i < cookies.length; i++) {
		let cookie = cookies[i].trimStart();
		if (cookie.startsWith(key)) {
			return cookie.substring(name.length, cookie.length);
		}
	}
	return null;
}

function deleteCookie(key) {
	setCookie(key, "", -1 * 24 * 60);
}

function fetchMakes(datalist) {
	fetchToDatalist(`${getRoot()}/scripts/php/fetchmakes.php`, datalist);
}

function fetchTypes(datalist) {
	fetchToDatalist(`${getRoot()}/scripts/php/fetchtypes.php`, datalist);
}

function fetchToDatalist(url, datalist) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				while (datalist.firstChild) {
					datalist.removeChild(datalist.lastChild);
				}
				response.data.forEach(function (type) {
					let child = document.createElement("option");
					child.setAttribute("value", type);
					datalist.appendChild(child);
				});
			}
			else {
				console.log(response);
			}
		}
	};
	request.open("GET", url, true);
	request.send();
}

function fetchIdempotencyToken(callback) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				callback(response.data);
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/genuuid.php?table=idempotency`, true);
	request.send();
}
