function getSession(uuid, callback) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			callback(response);
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/sessionmgmt.php?sid=${encodeURIComponent(uuid)}`, true);
	request.send();
}

function getUser(uuid, callback) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			callback(response);
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/usermgmt.php?id=${encodeURIComponent(uuid)}`, true);
	request.send();
}

function login(username, password, callback) {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			callback(response);
		}
	};
	let data = new FormData();
	data.append("uname", username);
	data.append("passwd", password);
	request.open("POST", `${getRoot()}/scripts/php/sessionmgmt.php`, true);
	request.send(data);
}

function getActiveSessionInfo(callback) {
	const response = {
		"user": null,
		"session": null
	};
	let session_id = getCookie("session_id");
	if (session_id == null) {
		callback(response);
		return;
	}
	let session_callback = function (session_response) {
		response.session = session_response.data;
		if (response.session == null) {
			deleteCookie("session_id");
			callback(response);
			return;
		}
		let user_id = response.session.user_id;
		let user_callback = function (user_response) {
			response.user = user_response.data;
			if (response.user == null) {
				deleteCookie("session_id");
			}
			callback(response);
		};
		getUser(user_id, user_callback);
	};
	getSession(session_id, session_callback);
}

function checkPermission(user, permission, callback) {
	if (user == null) {
		callback(false);
		return;
	}
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				callback(response.data.value);
				return;
			}
			callback(false);
		}
	};
	let user_id_encoded = encodeURIComponent(user.id);
	let permission_encoded = encodeURIComponent(permission);
	request.open(
		"GET",
		`${getRoot()}/scripts/php/permissionmgmt.php?uid=${user_id_encoded}&permission=${permission_encoded}`,
		true
	);
	request.send();
}
