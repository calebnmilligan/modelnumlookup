document.addEventListener("DOMContentLoaded", function () {
	document.getElementById("btn-delete-row").onclick = clickDelete;
	document.getElementById("btn-update-row").onclick = submitUpdate;
	
	refreshInfo();
	getActiveSessionInfo(function (session_info) {
		checkPermission(session_info.user, "entry.delete", function (value) {
			if (!value) {
				disableButtonDeleteEntry();
			}
		});
		checkPermission(session_info.user, "entry.create", function (value) {
			if (!value) {
				disableButtonUpdateEntry();
				disableEditing();
			}
		});
	});
	
	fetchIdempotencyToken(function (token) {
		window.idempotency_token = token;
	});
	refreshMakesAndModels();
});

function refreshInfo() {
	let id = getUrlParameter(window.location.href, "id", null);
	if (id === null) {
		return;
	}
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				document.getElementById("field-id").innerText = response.data.id;
				document.getElementById("field-model-number").value = response.data.model_number;
				document.getElementById("field-make").value = response.data.make;
				document.getElementById("field-model").value = response.data.model;
				document.getElementById("field-type").value = response.data.type;
				document.getElementById("field-base-price").value = response.data.condition_excellent;
				document.getElementById("field-price-excellent").innerText = response.data.condition_excellent;
				document.getElementById("field-price-very-good").innerText = response.data.condition_very_good;
				document.getElementById("field-price-good").innerText = response.data.condition_good;
				document.getElementById("field-price-fair").innerText = response.data.condition_fair;
				document.getElementById("field-price-poor-untested").innerText = response.data.condition_poor_untested;
				document.getElementById("field-price-not-working").innerText = response.data.condition_nonfunctional;
				let dummy_image = document.getElementById("image-dummy");
				let image_container = document.getElementById("container-images");
				if (response.data.image_ids.length === 0) {
					image_container.style.display = "none";
					image_container.style.visibility = "hidden";
				}
				else {
					while (image_container.firstChild) {
						image_container.removeChild(image_container.lastChild);
					}
					response.data.image_ids.forEach(function (image_id) {
						let clone = dummy_image.cloneNode(true);
						clone.removeAttribute("id");
						let link = getFirstChildOfType(clone, "a");
						let image_src = `${getRoot()}/scripts/php/image.php?id=${image_id}`;
						link.setAttribute("href", image_src);
						let image = getFirstChildOfType(link, "img");
						image.setAttribute("src", `${image_src}&thumb=large`);
						image.setAttribute("alt", `An image depicting: ${response.data.model}`);
						image_container.appendChild(clone);
					});
					image_container.style.display = "";
					image_container.style.visibility = "";
					document.getElementById("field-user-images").value = null;
				}
			}
		}
	};
	request.open("GET", `${getRoot()}/scripts/php/query.php?id=${id}`, true);
	request.send();
}

function sendDeleteRequest() {
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				alert("Entry deleted successfully");
				window.location.href = getRoot();
			}
			else {
				let messages = ["There was a problem processing your request"];
				if (response.error) {
					messages.push(response.error.message);
				}
				alert(messages.join("\n"));
			}
		}
	};
	let id = getUrlParameter(window.location.href, "id", null);
	request.open("DELETE", `${getRoot()}/scripts/php/query.php?rowid=${encodeURIComponent(id)}`, true);
	request.send();
}

function clickDelete() {
	let btn_delete = document.getElementById("btn-delete-row");
	if (btn_delete == null) {
		return;
	}
	let confirm = btn_delete.classList.contains("confirm");
	if (!confirm) {
		addClass(btn_delete, "confirm");
		btn_delete.value = "Click to confirm";
		return;
	}
	sendDeleteRequest();
	resetDelete(true);
}

function resetDelete() {
	let btn_delete = document.getElementById("btn-delete-row");
	if (btn_delete == null) {
		return;
	}
	removeClass(btn_delete, "confirm");
	btn_delete.value = "Delete";
}

function disableButtonDeleteEntry() {
	let elem = document.getElementById("row-delete");
	elem.parentNode.removeChild(elem);
}

function disableButtonUpdateEntry() {
	let elem = document.getElementById("row-update");
	elem.parentNode.removeChild(elem);
	elem = document.getElementById("row-attach-images");
	elem.parentNode.removeChild(elem);
}

function disableEditing() {
	let inputs = Array.from(document.getElementsByTagName("input"));
	for (let input of inputs) {
		let content = input.value;
		let span = document.createElement("span");
		span.innerText = content;
		[...input.attributes].forEach(function (attribute) {
			span.setAttribute(attribute.nodeName, attribute.nodeValue);
		});
		
		// Allow access of value property as if the element were an input
		Object.defineProperty(span, "value", {
			get: function () {
				return this.innerText;
			},
			set: function (value) {
				return this.innerText = value;
			}
		});
		
		input.replaceWith(span);
	}
}

function refreshMakesAndModels() {
	fetchMakes(document.getElementById("datalist-makes"));
	fetchTypes(document.getElementById("datalist-types"));
}

function submitUpdate() {
	let form_data = new FormData();
	form_data.append("id", getUrlParameter(window.location.href, "id", null));
	form_data.append("model_number", document.getElementById("field-model-number").value);
	form_data.append("make", document.getElementById("field-make").value);
	form_data.append("model", document.getElementById("field-model").value);
	form_data.append("type", document.getElementById("field-type").value);
	form_data.append("base_price", document.getElementById("field-base-price").value);
	form_data.append("idempotency", window.idempotency_token);
	let files = document.getElementById("field-user-images").files;
	for (let i = 0; i < files.length; i++) {
		form_data.append("user_image[]", files[i]);
	}
	
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				alert("Entry updated successfully");
				refreshInfo();
				refreshMakesAndModels();
				fetchIdempotencyToken(function (token) {
					window.idempotency_token = token;
				});
			}
			else {
				let messages = ["There was a problem processing your request"];
				if (response.error) {
					messages.push(response.error.message);
				}
				alert(messages);
			}
		}
	};
	request.open("POST", `${getRoot()}/scripts/php/newentry.php`, true);
	request.send(form_data);
}
