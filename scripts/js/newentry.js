function submitNew() {
	let form_data = new FormData();
	form_data.append("model_number", document.getElementById("input-model-number").value);
	form_data.append("make", document.getElementById("input-make").value);
	form_data.append("model", document.getElementById("input-model").value);
	form_data.append("type", document.getElementById("input-type").value);
	form_data.append("base_price", document.getElementById("input-base-price").value);
	form_data.append("idempotency", window.idempotency_token);
	let files = document.getElementById("input-image").files;
	for (let i = 0; i < files.length; i++) {
		form_data.append("user_image[]", files[i]);
	}
	
	let request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			let response = JSON.parse(this.responseText);
			if (response.success) {
				document.getElementById("error-container").style.display = "none";
				document.getElementById("form-new").reset();
				refreshMakesAndModels();
				fetchIdempotencyToken(function (token) {
					window.idempotency_token = token;
				});
			}
			else {
				let messages = ["There was a problem processing your request"];
				if (response.error) {
					messages.push(response.error.message);
				}
				showErrors(messages);
				document.getElementById("error-container").style.display = "block";
			}
		}
	};
	request.open("POST", `${getRoot()}/scripts/php/newentry.php`, true);
	request.send(form_data);
}

function refreshMakesAndModels() {
	fetchMakes(document.getElementById("datalist-makes"));
	fetchTypes(document.getElementById("datalist-types"));
}

document.addEventListener("DOMContentLoaded", function () {
	document.getElementById("form-new").onsubmit = function () {
		submitNew();
		return false;
	};
	refreshMakesAndModels();
	fetchIdempotencyToken(function (token) {
		window.idempotency_token = token;
	});
});
