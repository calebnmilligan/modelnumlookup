function showErrors(messages) {
	let error_container = document.getElementById("error-container");
	while (error_container.firstChild) {
		error_container.removeChild(error_container.firstChild);
	}
	messages.forEach(function (message) {
		let node = document.createElement("p");
		node.innerText = message;
		error_container.appendChild(node);
	});
}