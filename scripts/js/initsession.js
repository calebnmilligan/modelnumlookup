document.addEventListener("DOMContentLoaded", populateNavLinks);

function populateNavLinks() {
	getActiveSessionInfo(function (session_info) {
		const user = session_info.user;
		let user_link = document.getElementById("user-link");
		let username_references = Array.from(document.getElementsByClassName("username"));
		if (user == null) {
			user_link.innerText = "Sign in";
			user_link.setAttribute("href", `${getRoot()}/user/login/`);
			username_references.forEach(function (item) {
				item.innerText = "You are not signed in.";
			});
		}
		else {
			user_link.innerText = "Sign out";
			user_link.setAttribute("href", `${getRoot()}/user/logout/`);
			username_references.forEach(function (item) {
				item.innerText = user.username;
			});
		}
	});
}
