<?php
include_once "conn.php";
include_once "Result.php";

$result = new Result();

try {
	$conn = getDBConnection();
	$stmt = $conn->prepare("SELECT DISTINCT(`make`) FROM `model_lookup` WHERE `make` IS NOT NULL ORDER BY `make`");
	$stmt->execute();
	$result->data = $stmt->fetchAll(PDO::FETCH_COLUMN);
}
catch (Exception $e) {
	$result->setFailed(500, $e);
	error_log($e);
}
$result->sendHttpResponse();
