<?php
include_once "conn.php";
include_once "User.php";
include_once "PermissionGroup.php";
include_once "Session.php";
include_once "Result.php";

function queryId(Result &$result, int $id): Result {
	try {
		$db = getDBConnection();
		$stmt = $db->prepare("SELECT `id`, `model_number`, `make`, `model`, `type`, `condition_excellent`, "
			. "`condition_very_good`, `condition_good`, `condition_fair`, `condition_poor_untested`, "
			. "`condition_nonfunctional` FROM `model_lookup_master` WHERE `id`=:id");
		$stmt->bindParam(":id", $id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$result->data = $stmt->fetch(PDO::FETCH_ASSOC);
			foreach ($result->data as $key => $value) {
				if (str_starts_with($key, "condition_")) {
					$result->data[$key] = intval($value);
				}
			}
		}
		$stmt =
			$db->prepare("SELECT `image_id` FROM `model_lookup_images` WHERE `model_id`=:id ORDER BY `order`, `image_id` ASC");
		$stmt->bindParam(":id", $id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$result->data["image_ids"] = $stmt->fetchAll(PDO::FETCH_COLUMN);
		}
	}
	catch (Exception $e) {
		$result->setFailed(500, $e);
		error_log($e);
	}
	return $result;
}

function query(Result &$result, string $field, string $query, int $count = -1, int $page = 0): Result {
	$valid_fields = ["make", "model", "model_number", "type", "all"];
	$field = strtolower($field);
	if (!in_array($field, $valid_fields)) {
		$result->setFailed(400, "Invalid field");
	}
	else {
		try {
			$query_empty = empty(trim($query));
			$comparison = $query_empty ? "IS NULL" : "LIKE :q";
			$limit = $count <= 0 ? "" : "LIMIT :limit OFFSET :offset";
			$raw_query = "SELECT `id`, `model_number`, `make`, `model`, `type`, `condition_excellent`, "
				. "`condition_very_good`, `condition_good`, `condition_fair`, `condition_poor_untested`, "
				. "`condition_nonfunctional` FROM `model_lookup_master` WHERE ";
			$count_query = "SELECT COUNT(*) FROM `model_lookup` WHERE ";
			if ($field === "all") {
				$all_fields =
					"`model_number` $comparison OR `make` $comparison OR `model` $comparison OR `type` $comparison";
				$raw_query .= $all_fields;
				$count_query .= $all_fields;
			}
			else {
				$raw_query .= "`$field` $comparison";
				$count_query .= "`$field` $comparison";
			}
			if ($count > 0) {
				$raw_query .= " $limit";
			}
			$conn = getDBConnection();
			$stmt = $conn->prepare($raw_query);
			if (!$query_empty) {
				$stmt->bindParam(":q", $query);
			}
			if ($count > 0) {
				$offset = $page * $count;
				$stmt->bindParam(":limit", $count, PDO::PARAM_INT);
				$stmt->bindParam(":offset", $offset, PDO::PARAM_INT);
			}
			if ($stmt->execute()) {
				$result->data = [
					"count" => $count,
					"page" => $page,
					"pages" => 1,
					"items" => $stmt->fetchAll(PDO::FETCH_ASSOC)
				];
				if ($count > 0) {
					$stmt = $conn->prepare($count_query);
					if (!$query_empty) {
						$stmt->bindParam(":q", $query);
					}
					if ($stmt->execute()) {
						$result->data["pages"] = ceil($stmt->fetchColumn(0) / $count);
					}
				}
				
				foreach ($result->data["items"] as $rownum => $row) {
					foreach ($row as $key => $value) {
						if (str_starts_with($key, "condition_")) {
							$row[$key] = intval($value);
						}
					}
					$result->data["items"][$rownum] = $row;
				}
			}
			else {
				$result->success = false;
			}
		}
		catch (Exception $e) {
			$result->setFailed(500, $e);
			error_log($e);
		}
	}
	return $result;
}


$result = new Result(200, true);

if ($_SERVER["REQUEST_METHOD"] === "GET") {
	if (isset($_GET["id"])) {
		$id = $_GET["id"];
		queryId($result, $id);
	}
	else if (!isset($_GET["field"])) {
		$result->setFailed(400, "Missing parameter \"field\"");
	}
	else if (!isset($_GET["query"])) {
		$result->setFailed(400, "Missing parameter \"query\"");
	}
	else {
		$field = $_GET["field"];
		$query = $_GET["query"];
		$count = intval($_GET["count"] ?? 0);
		$page = intval($_GET["page"] ?? 0);
		query($result, $field, $query, $count, $page);
	}
}
else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {
	if (!isset($_COOKIE["session_id"])) {
		$result->setFailed(401, "Not signed in");
	}
	else {
		$session_id = $_COOKIE["session_id"];
		$session = Session::getSession($session_id);
		if ($session === null) {
			$result->setFailed(401, "Invalid session");
		}
		else {
			$user = User::loadUser($session->user_id);
			$has_permission = PermissionGroup::anyGroupHasPermission($user->permission_groups, "entry.delete");
			if ($has_permission !== true) {
				$result->setFailed(401, "Insufficient permissions");
			}
			else {
				if (!isset($_GET["rowid"])) {
					$result->setFailed(400, "Missing parameter \"rowid\"");
				}
				else {
					$conn = getDBConnection();
					$stmt = $conn->prepare("DELETE FROM `model_lookup` WHERE `id` = :rowid");
					$stmt->bindParam(":rowid", $_GET["rowid"], PDO::PARAM_INT);
					$result->success = $stmt->execute();
				}
			}
		}
	}
}
else {
	$result->setFailed(405, "Invalid method (Expected GET or DELETE)");
	header("Allow:GET,DELETE");
}
$result->sendHttpResponse();
