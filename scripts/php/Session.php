<?php
include_once "UUID.php";
include_once "conn.php";
include_once "pubenv.php";

class Session {
	public string $id;
	public string $user_id;
	public int $started_at;
	public int $expires_at;
	
	private function __construct(UUID|string $id, UUID|string $user_id, int $started_at, int $expires_at) {
		$this->id = (new UUID($id))->getStringRep();
		$this->user_id = (new UUID($user_id))->getStringRep();
		$this->started_at = $started_at;
		$this->expires_at = $expires_at;
	}
	
	public static function extendSession(UUID|string $session_id, ?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		$new_expire = time() + ($_ENV["session_expire_minutes"] * 60);
		setcookie("session_id", $session_id, $new_expire, "/");
		$stmt = $db->prepare("UPDATE `sessions` SET `expires_at`=FROM_UNIXTIME(:expires) WHERE `id`=:sid");
		$stmt->bindParam(":expires", $new_expire, PDO::PARAM_INT);
		$stmt->bindParam(":sid", $session_id, PDO::PARAM_STR);
		$stmt->execute();
	}
	
	public static function createSession(UUID|string $user_id, ?PDO $db = null): Session {
		if ($db == null) {
			$db = getDBConnection();
		}
		$session_id = UUID::genUuidV4("sessions");
		$started_at = time();
		$expires_at = $started_at + ($_ENV["session_expire_minutes"] * 60);
		$stmt = $db->prepare("INSERT INTO `sessions` (`id`, `user_id`, `started_at`, `expires_at`) VALUES "
			. "(:sid, :uid, FROM_UNIXTIME(:time), FROM_UNIXTIME(:expires))");
		$stmt->bindParam(":sid", $session_id, PDO::PARAM_STR);
		$stmt->bindParam(":uid", $user_id, PDO::PARAM_STR);
		$stmt->bindParam(":time", $started_at, PDO::PARAM_INT);
		$stmt->bindParam(":expires", $expires_at, PDO::PARAM_INT);
		$stmt->execute();
		return new Session($session_id, $user_id, $started_at, $expires_at);
	}
	
	public static function getSession(UUID|string $session_id, ?PDO $db = null): ?Session {
		if ($db == null) {
			$db = getDBConnection();
		}
		self::purgeSessions($db);
		$stmt = $db->prepare("SELECT `user_id`, UNIX_TIMESTAMP(`started_at`) AS `started_at`, "
			. "UNIX_TIMESTAMP(`expires_at`) AS `expires_at` FROM `sessions` WHERE `id`=:sid");
		$stmt->bindParam(":sid", $session_id, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result === false) {
			return null;
		}
		$expires_at = $result["expires_at"];
		if (time() >= $expires_at) {
			self::endSession($session_id, $db);
			return null;
		}
		return new Session($session_id, $result["user_id"], $result["started_at"], $result["expires_at"]);
	}
	
	public static function purgeSessions(?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("DELETE FROM `sessions` WHERE `expires_at` < FROM_UNIXTIME(:now)");
		$now = time();
		$stmt->bindParam(":now", $now, PDO::PARAM_INT);
		$stmt->execute();
	}
	
	public static function endSession(UUID|string $session_id, ?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("DELETE FROM `sessions` WHERE `id`=:sid");
		$stmt->bindParam(":sid", $session_id, PDO::PARAM_STR);
		$stmt->execute();
	}
	
	public static function getSessionsForUser(UUID|string $user_id, ?PDO $db = null): array {
		if ($db == null) {
			$db = getDBConnection();
		}
		self::purgeSessions($db);
		$stmt = $db->prepare("SELECT `id`, UNIX_TIMESTAMP(`started_at`) AS `started_at`, UNIX_TIMESTAMP(`expires_at`) "
			. "AS `expires_at` FROM `sessions` WHERE `user_id`=:uid");
		$stmt->bindParam(":uid", $user_id, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$sessions = [];
		$now = time();
		foreach ($result as $row) {
			if ($now >= $row["expires_at"]) {
				self::endSession($row["id"], $db);
			}
			else {
				$sessions[] = new Session($row["id"], $user_id, $row["started_at"], $row["expires_at"]);
			}
		}
		return $sessions;
	}
	
	public function end(?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		self::endSession($this->id, $db);
	}
}
