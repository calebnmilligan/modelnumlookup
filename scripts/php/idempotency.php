<?php
include_once "conn.php";
include_once "Result.php";
include_once "UUID.php";

function storeResponse(string $token, int $status_code, string $response, array $headers = null, ?PDO $db = null): void {
	if ($db == null) {
		$db = getDBConnection();
	}
	if ($headers === null) {
		$headers = headers_list();
	}
	$encoded_headers = [];
	if ($headers !== null) {
		foreach ($headers as $header) {
			$encoded_headers[] = urlencode($header);
		}
	}
	$header_string = implode(";", $encoded_headers);
	$now = time();
	$stmt =
		$db->prepare("INSERT INTO `idempotency` (`id`, `timestamp`, `status_code`, `headers`, `response`) VALUES (:id, FROM_UNIXTIME(:now), :status_code, :headers, :response)");
	$stmt->bindParam(":id", $token, PDO::PARAM_STR);
	$stmt->bindParam(":now", $now, PDO::PARAM_INT);
	$stmt->bindParam(":status_code", $status_code, PDO::PARAM_STR);
	$stmt->bindParam(":headers", $header_string, PDO::PARAM_STR);
	$stmt->bindParam(":response", $response, PDO::PARAM_STR);
	$stmt->execute();
}

function fetchResponse(string $token, ?PDO $db = null): bool {
	if ($db == null) {
		$db = getDBConnection();
	}
	purgeIdempotency($db);
	$stmt = $db->prepare("SELECT `status_code`, `headers`, `response` FROM `idempotency` WHERE `id`=:id");
	$stmt->bindParam(":id", $token, PDO::PARAM_STR);
	$stmt->execute();
	$query_result = $stmt->fetch(PDO::FETCH_ASSOC);
	if ($query_result === false) {
		return false;
	}
	else {
		http_response_code($query_result["status_code"]);
		$encoded_headers = explode(";", $query_result["headers"]);
		foreach ($encoded_headers as $encoded_header) {
			header(urldecode($encoded_header));
		}
		print($query_result["response"]);
	}
	return true;
}

function purgeIdempotency($db = null): void {
	if ($db === null) {
		$db = getDBConnection();
	}
	$stmt = $db->prepare("DELETE FROM `idempotency` WHERE `timestamp` < FROM_UNIXTIME(:now) - INTERVAL 1 DAY");
	$now = time();
	$stmt->bindParam(":now", $now, PDO::PARAM_INT);
	$stmt->execute();
}
