<?php
include_once "UUID.php";
include_once "Result.php";

$result = new Result();
$result->data = [];

try {
	if ($_SERVER["REQUEST_METHOD"] === "GET") {
		$table = $_REQUEST["table"] ?? null;
		$result->data = UUID::genUuidV4($table)->getStringRep();
		if ($result->data === null) {
			$result->setFailed(500, "Failed to generate UUID");
		}
	}
	else {
		$result->setFailed(405);
		header("Allow:GET");
	}
}
catch (Exception $e) {
	$result->setFailed(500, $e);
	error_log($e);
}
header("Cache-Control: no-store");
$result->sendHttpResponse();
