<?php
include_once "env.php";

function getDBConnection(bool $autocommit = true): PDO {
	$db_host = $_ENV["db_host"];
	$db_port = $_ENV["db_port"];
	$db_user = $_ENV["db_user"];
	$db_pass = $_ENV["db_pass"];
	$db_name = $_ENV["db_name"];
	$conn = new PDO("mysql:host=$db_host;port=$db_port;dbname=$db_name;charset=utf8mb4",
		$db_user,
		$db_pass,
		[PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"]);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->setAttribute(PDO::ATTR_AUTOCOMMIT, $autocommit);
	$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	return $conn;
}
