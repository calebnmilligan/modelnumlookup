<?php
include_once "conn.php";

class PermissionGroup {
	public int $id;
	public string $name;
	public ?string $parent_id;
	public array $permissions;
	
	/**
	 * PermissionGroup constructor.
	 * @param $id int
	 * @param $name string
	 * @param string|null $parent_id string
	 * @param $permissions array
	 */
	private function __construct(int $id, string $name, ?int $parent_id, array $permissions) {
		$this->id = $id;
		$this->name = $name;
		$this->parent_id = $parent_id;
		$this->permissions = $permissions;
	}
	
	/**
	 * @param $groups array
	 * @param $permission string
	 * @return bool|null
	 */
	public static function anyGroupHasPermission(array $groups, string $permission): ?bool {
		$value = null;
		foreach ($groups as $group) {
			$loc_value = self::getGroup($group)->getWildcardPermission($permission);
			if ($value === null) {
				$value = $loc_value;
			}
			else {
				$value &= $loc_value;
			}
		}
		return $value;
	}
	
	/**
	 * @param $permission_name string
	 * @param $checked array;
	 * @return boolean|null
	 */
	public function getWildcardPermission(string $permission_name, array &$checked = []): ?bool {
		if (in_array($this->name, $checked)) {
			return false;
		}
		$checked[] = $this->name;
		$res = $this->getExactPermission($permission_name) || $this->getExactPermission("*");
		if ($res === true) {
			return true;
		}
		
		$val = false;
		$parts = explode(".", $permission_name);
		$curstr = "";
		foreach ($parts as $part) {
			$curstr .= $part . ".";
			$curwild = $curstr . "*";
			$res = $this->getExactPermission($curwild);
			if ($res !== null) {
				$val = $res;
			}
		}
		return $val;
	}
	
	/**
	 * @param $permission_name string
	 * @param $checked array;
	 * @return boolean|null
	 */
	public function getExactPermission(string $permission_name, array &$checked = []): ?bool {
		if (in_array($this->name, $checked)) {
			return null;
		}
		$checked[] = $this->name;
		
		if ($this->isPermissionSet($permission_name)) {
			return $this->permissions[$permission_name];
		}
		
		if ($this->parent_id !== null) {
			$parent_group = self::getGroupById($this->parent_id);
			if ($parent_group !== null) {
				return $parent_group->getExactPermission($permission_name, $checked);
			}
		}
		return null;
	}
	
	/**
	 * @param $permission_name string
	 * @return boolean
	 */
	public function isPermissionSet(string $permission_name): bool {
		return isset($this->permissions[$permission_name]);
	}
	
	/**
	 * @param $name string
	 * @return PermissionGroup|null
	 */
	public static function getGroup(string $name): ?PermissionGroup {
		foreach (self::loadGroups() as $group) {
			if ($group->name === $name) {
				return $group;
			}
		}
		return null;
	}
	
	/**
	 * @param $id int
	 * @return PermissionGroup|null
	 */
	public static function getGroupById(int $id): ?PermissionGroup {
		foreach (self::loadGroups() as $group) {
			if ($group->id === $id) {
				return $group;
			}
		}
		return null;
	}
	
	/**
	 * @param bool $force
	 * @return array
	 */
	public static function loadGroups(bool $force = false): array {
		$db = getDBConnection();
		$stmt = $db->prepare("SELECT `id`, `name`, `parent_id` FROM `permission_groups`");
		$stmt->execute();
		$raw_groups = $stmt->fetchAll(PDO::FETCH_ASSOC);
		static $loc_groups = [];
		if ($force || empty($loc_groups)) {
			foreach ($raw_groups as $raw_group) {
				$group_id = $raw_group["id"];
				$stmt =
					$db->prepare("SELECT `permission_name`, `value` FROM `permission_group_permissions` WHERE `group_id`=:group_id");
				$stmt->bindParam(":group_id", $group_id, PDO::PARAM_INT);
				$stmt->execute();
				$raw_permissions = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$permissions = [];
				foreach ($raw_permissions as $raw_permission) {
					$permissions[$raw_permission["permission_name"]] = (boolean)$raw_permission["value"];
				}
				$loc_groups[] = new PermissionGroup($group_id, $raw_group["name"], $raw_group["parent_id"], $permissions);
			}
		}
		return $loc_groups;
	}
}
