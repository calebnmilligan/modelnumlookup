<?php
include_once "conn.php";
include_once "User.php";
include_once "PermissionGroup.php";
include_once "Session.php";
include_once "Result.php";
include_once "idempotency.php";
include_once "DBImage.php";

$result = new Result();

if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SERVER["REQUEST_METHOD"] == "PUT") {
	if (!isset($_POST["idempotency"])) {
		$result->setFailed(400, "Missing parameter \"idempotency\"");
		$result->sendHttpResponse();
		return;
	}
	$idempotency_key = $_POST["idempotency"];
	if (!preg_match("/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i", $idempotency_key)) {
		$result->setFailed(400, "Invalid idempotency key");
		$result->sendHttpResponse();
		return;
	}
	
	$conn = getDBConnection(false);
	
	if (fetchResponse($idempotency_key, $conn)) {
		return;
	}
	try {
		if (!isset($_COOKIE["session_id"])) {
			$result->setFailed(401, "Not signed in");
		}
		else {
			$session_id = $_COOKIE["session_id"];
			$session = Session::getSession($session_id, $conn);
			if ($session === null) {
				$result->setFailed(401, "Invalid session");
			}
			else {
				$user = User::loadUser($session->user_id, $conn);
				$has_permission = PermissionGroup::anyGroupHasPermission($user->permission_groups, "entry.create");
				if ($has_permission !== true) {
					$result->setFailed(403, "Insufficient permissions");
				}
				else {
					if (!isset($_POST["model"])) {
						$result->setFailed(400, "Missing parameter \"model\"");
					}
					else if (trim($_POST["model"]) === "") {
						$result->setFailed(400, "Parameter \"model\" cannot be empty");
					}
					else if (!isset($_POST["type"])) {
						$result->setFailed(400, "Missing parameter \"type\"");
					}
					else if (trim($_POST["type"]) === "") {
						$result->setFailed(400, "Parameter \"type\" cannot be empty");
					}
					else if (!isset($_POST["base_price"])) {
						$result->setFailed(400, "Missing parameter \"base_price\"");
					}
					else if (trim($_POST["base_price"]) === "") {
						$result->setFailed(400, "Parameter \"base_price\" cannot be empty");
					}
					else if (!is_numeric($_POST["base_price"])) {
						$result->setFailed(400, "Parameter \"base_price\" must be numeric");
					}
					else {
						$id = $_POST["id"] ?? null;
						$model_number = isset($_POST["model_number"]) && strlen(trim($_POST["model_number"])) != 0 ?
							trim($_POST["model_number"]) : null;
						$make = isset($_POST["make"]) && strlen(trim($_POST["make"])) != 0 ? trim($_POST["make"]) :
							null;
						$model = trim($_POST["model"]);
						$type = trim($_POST["type"]);
						$base_price = $_POST["base_price"];
						
						$stmt = $id === null
							?
							$conn->prepare("INSERT INTO `model_lookup` (`model_number`, `make`, `model`, `type`, `base_price`) VALUES (:mnum, :make, :model, :type, :bprice)")
							:
							$conn->prepare("UPDATE `model_lookup` SET `model_number`=:mnum, `make`=:make, `model`=:model, `type`=:type, `base_price`=:bprice WHERE `id`=:id");
						$stmt->bindParam(":mnum", $model_number);
						$stmt->bindParam(":make", $make);
						$stmt->bindParam(":model", $model);
						$stmt->bindParam(":type", $type);
						$stmt->bindParam(":bprice", $base_price);
						if ($id !== null) {
							$stmt->bindParam(":id", $id);
						}
						$stmt->execute();
						$model_id = $id ?? $conn->lastInsertId();
						
						if (isset($_FILES["user_image"])) {
							$num_files = sizeof($_FILES["user_image"]["name"]);
							for ($i = 0; $i < $num_files; $i++) {
								$name = $_FILES["user_image"]["name"][$i];
								$mime_type = $_FILES["user_image"]["type"][$i];
								$image = file_get_contents($_FILES["user_image"]["tmp_name"][$i]);
								$db_image = DBImage::storeImage(
									$name,
									$user->id,
									null,
									$mime_type,
									$_FILES["user_image"]["tmp_name"][$i],
									$conn);
								if ($db_image !== null) {
									$image_id = $db_image->getId();
									$stmt =
										$conn->prepare("INSERT INTO `model_lookup_images` (`model_id`, `image_id`, `order`) "
											. "VALUES (:model_id, :image_id, :order)");
									$stmt->bindParam(":model_id", $model_id, PDO::PARAM_INT);
									$stmt->bindParam(":image_id", $image_id, PDO::PARAM_STR);
									$stmt->bindParam(":order", $i, PDO::PARAM_INT);
									$stmt->execute();
								}
							}
						}
					}
				}
			}
		}
	}
	catch (Exception $e) {
		$result->setFailed(500, $e);
		error_log($e);
	}
	if ($result->success && $idempotency_key !== null) {
		storeResponse($idempotency_key, $result->status, $result->toJsonString(), null, $conn);
	}
	$conn->commit();
}
else {
	$result->setFailed(405, "Invalid method (Expected POST or PUT)");
	header("Allow:POST,PUT");
}
$result->sendHttpResponse();
