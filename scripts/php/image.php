<?php
include_once "Result.php";
include_once "DBImage.php";
include_once "Session.php";
include_once "User.php";
include_once "PermissionGroup.php";

$result = new Result(200, true);

if ($_SERVER["REQUEST_METHOD"] === "GET") {
	if (!isset($_GET["id"])) {
		$result->setFailed(400, "Missing parameter \"id\"");
	}
	$image_id = $_GET["id"];
	if (!preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i", $image_id)) {
		$result->setFailed(400, "Invalid image ID");
		$result->sendHttpResponse();
		return;
	}
	$thumb_sizes = [0 => "full", 1 => "large", 2 => "medium", 3 => "small"];
	$thumb_size = -1;
	if (isset($_GET["thumb"])) {
		foreach ($thumb_sizes as $key => $value) {
			if (strtolower($_GET["thumb"]) == strtolower($value)) {
				$thumb_size = $key;
				break;
			}
		}
		if ($thumb_size < 0) {
			$result->setFailed(400, "Thumb size must be one of: full, large, medium, small");
			$result->sendHttpResponse();
			return;
		}
	}
	else {
		$thumb_size = 0;
	}
	
	$image = DBImage::fetchImage($image_id, $thumb_size);
	
	if ($image === null) {
		$result->setFailed(404, "No image exists with ID " . $image_id);
		$result->sendHttpResponse();
		return;
	}
	
	$ext = pathinfo($image->getName(), PATHINFO_EXTENSION);
	$name = ($thumb_size > 0 ? $image->getId() . "_" . $thumb_sizes[$thumb_size] : $image->getId()) . "." . $ext;
	
	$length = strlen($image->getImage());
	
	header("Content-Type: " . $image->getMimeType());
	header("Content-Disposition: inline; filename=\"" . $name . "\"");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: " . $length);
	header("Cache-control: max-age=" . (60 * 60 * 24 * 365));
	header("Expires: " . gmdate(DATE_RFC1123, time() + 60 * 60 * 24 * 365));
	header("Pragma: public");
	print($image->getImage());
	return;
}
else if ($_SERVER["REQUEST_METHOD"] === "POST") {
	if (!isset($_COOKIE["session_id"])) {
		$result->setFailed(401, "Not signed in");
	}
	else {
		$session_id = $_COOKIE["session_id"];
		$session = Session::getSession($session_id);
		if ($session === null) {
			$result->setFailed(401, "Invalid session");
		}
		else {
			$user = User::loadUser($session->user_id);
			$has_permission = PermissionGroup::anyGroupHasPermission($user->permission_groups, "image.create");
			if ($has_permission !== true) {
				$result->setFailed(401, "Insufficient permissions");
			}
			else {
				if (!isset($_FILES["user_image"])) {
					$result->setFailed(400, "Missing file \"user_image\"");
				}
				else {
					$name = $_FILES["user_image"]["name"];
					$mime_type = $_FILES["user_image"]["type"];
					$result->data =
						DBImage::storeImage($name, $user->id, null, $mime_type, $_FILES["user_image"]["tmp_name"]);
				}
			}
		}
	}
	$result->sendHttpResponse();
	return;
}
else if ($_SERVER["REQUEST_METHOD"] === "DELETE") {
	if (!isset($_COOKIE["session_id"])) {
		$result->setFailed(401, "Not signed in");
	}
	else {
		$session_id = $_COOKIE["session_id"];
		$session = Session::getSession($session_id);
		if ($session === null) {
			$result->setFailed(401, "Invalid session");
		}
		else {
			$user = User::loadUser($session->user_id);
			$has_permission = PermissionGroup::anyGroupHasPermission($user->permission_groups, "image.delete");
			if ($has_permission !== true) {
				$result->setFailed(401, "Insufficient permissions");
			}
			else {
				if (!isset($_REQUEST["id"])) {
					$result->setFailed(400, "Missing parameter \"id\"");
				}
				else {
					$conn = getDBConnection();
					$stmt = $conn->prepare("DELETE FROM `images` WHERE `id` = :id");
					$stmt->bindParam(":id", $_REQUEST["id"], PDO::PARAM_STR);
					$result->success = $stmt->execute();
				}
			}
		}
	}
	$result->sendHttpResponse();
	return;
}
else {
	$result->setFailed(405, "Invalid method (Expected GET, POST, or DELETE)");
	header("Allow:GET,POST,DELETE");
	$result->sendHttpResponse();
}
