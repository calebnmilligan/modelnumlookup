<?php

function findDocRoot(string $dir = __DIR__): ?string {
	$dir = str_replace('\\', '/', $dir);
	$files = scandir($dir);
	
	if (in_array(".docroot", $files)) {
		return $dir;
	}
	$parent = dirname($dir);
	if ($parent == $dir) {
		return null;
	}
	return findDocRoot($parent);
}

function getDocRootRelative(string $file = __FILE__): ?string {
	$file = str_replace('\\', '/', $file);
	
	$root = findDocRoot(is_dir($file) ? $file : dirname($file));
	if ($root === null) {
		return null;
	}
	return substr($file, strlen($root));
}

function getReturnToRoot(string $file = __FILE__): string {
	$relative = getDocRootRelative($file);
	print($relative . PHP_EOL);
	if ($relative === null) {
		return ".";
	}
	return str_repeat("../", substr_count($relative, "/"));
}
