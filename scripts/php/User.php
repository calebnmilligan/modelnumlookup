<?php
include_once "conn.php";
include_once "UUID.php";
include_once "env.php";

class User {
	public string $id;
	public string $username;
	public string $email;
	public bool $email_verified;
	public bool $active = false;
	public array $permission_groups = [];
	
	/**
	 * User constructor.
	 * @param $id UUID|string
	 * @param $username string
	 * @param $email string
	 * @param $email_verified bool
	 * @param $active bool
	 * @param $permission_groups array
	 */
	private function __construct(UUID|string $id, string $username, string $email, bool $email_verified, bool $active,
								 array       $permission_groups = []) {
		$this->id = (new UUID($id))->getStringRep();
		$this->username = $username;
		$this->email = $email;
		$this->email_verified = $email_verified;
		$this->active = $active;
		$this->permission_groups = $permission_groups;
	}
	
	/**
	 * @param $username string
	 * @param $email string
	 * @param $password string
	 * @return User|null
	 * @throws PDOException
	 * @throws EmailInUseException
	 * @throws UsernameInUseException
	 * @throws Exception
	 */
	public static function createUser(string $username, string $email, string $password, ?PDO $db = null): ?User {
		if ($db == null) {
			$db = getDBConnection();
		}
		if (static::usernameExists($username, $db)) {
			throw new UsernameInUseException();
		}
		if (static::emailExists($email, $db)) {
			throw new EmailInUseException();
		}
		$salt = random_bytes(32);
		$password = static::hashPassword($password, $salt);
		$uuid = UUID::genUuidv4("users", $db);
		$stmt = $db->prepare("INSERT INTO `users` (`id`, `username`, `email`, `password_hash`, `password_salt`) "
			. "VALUES (:id, :username, :email, :password_hash, :password_salt)");
		$stmt->bindParam(":id", $uuid, PDO::PARAM_STR);
		$stmt->bindParam(":username", $username, PDO::PARAM_STR);
		$stmt->bindParam(":email", $email, PDO::PARAM_STR);
		$stmt->bindParam(":password_hash", $password, PDO::PARAM_LOB);
		$stmt->bindParam(":password_salt", $salt, PDO::PARAM_LOB);
		$stmt->execute();
		return self::loadUser($uuid, $db);
	}
	
	/**
	 * @param $username string
	 * @return bool
	 * @throws PDOException
	 */
	public static function usernameExists(string $username, ?PDO $db = null): bool {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT COUNT(*) FROM `users` WHERE LOWER(`username`)=LOWER(:username)");
		$stmt->bindParam(":username", $username, PDO::PARAM_STR);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_COLUMN);
		return (bool)$results[0];
	}
	
	/**
	 * @param $email string
	 * @return bool
	 * @throws PDOException
	 */
	public static function emailExists(string $email, ?PDO $db = null): bool {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT COUNT(*) FROM `users` WHERE LOWER(`email`)=LOWER(:email)");
		$stmt->bindParam(":email", $email, PDO::PARAM_STR);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_COLUMN);
		return (bool)$results[0];
	}
	
	/**
	 * @param $password string
	 * @param $salt string
	 * @return string
	 */
	public static function hashPassword(string $password, string $salt): string {
		return hash_pbkdf2("sha512", $password, $salt, 65535, 64, true);
	}
	
	public static function loadUser(UUID|string $user_id, ?PDO $db = null): ?User {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT `username`, `email`, `email_verified`, `password_hash`, "
			. "`password_salt`, `active` FROM `users` WHERE `id`=:uid");
		$stmt->bindParam(":uid", $user_id, PDO::PARAM_STR);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($results) {
			$user = $results[0];
			$groups = self::loadUserPermissionGroups($user_id, $db);
			return new User($user_id,
				$user["username"],
				$user["email"],
				$user["email_verified"],
				$user["active"],
				$groups);
		}
		return null;
	}
	
	/**
	 * @param $user_id
	 * @param PDO|null $db
	 * @return array
	 */
	public static function loadUserPermissionGroups($user_id, ?PDO $db = null): array {
		if ($db == null) {
			$db = getDBConnection();
		}
		$groups = [];
		$stmt = $db->prepare("SELECT `permission_groups`.`name` FROM `permission_group_users` INNER JOIN "
			. "`permission_groups` ON `permission_groups`.`id` = `permission_group_users`.`group_id` "
			. "WHERE `user_id`=:uid");
		$stmt->bindParam(":uid", $user_id, PDO::PARAM_STR);
		$stmt->execute();
		while (($val = $stmt->fetch(PDO::FETCH_COLUMN, 0)) !== false) {
			$groups[] = $val;
		}
		return $groups;
	}
	
	/**
	 * @param $username_or_email string
	 * @param $password string
	 * @param PDO|null $db
	 * @return User
	 * @throws InvalidCredentialsException
	 */
	public static function loginUser(string $username_or_email, string $password, ?PDO $db = null): User {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT `id`, `username`, `email`, `email_verified`, `password_hash`, "
			. "`password_salt`, `active` FROM `users` WHERE LOWER(`email`)=LOWER(:username) "
			. "OR LOWER(`username`)=LOWER(:username)");
		$stmt->bindParam(":username", $username_or_email, PDO::PARAM_STR);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($results) {
			$user = $results[0];
			$check_hash = static::hashPassword($password, $user["password_salt"]);
			if (!self::slowEquals($check_hash, $user["password_hash"])) {
				throw new InvalidCredentialsException();
			}
			$groups = self::loadUserPermissionGroups($user["id"], $db);
			return new User(new UUID($user["id"]),
				$user["username"],
				$user["email"],
				$user["email_verified"],
				$user["active"],
				$groups);
		}
		throw new InvalidCredentialsException();
	}
	
	/**
	 * @param string $s1
	 * @param string $s2
	 * @return bool
	 */
	private static function slowEquals(string $s1, string $s2): bool {
		$a1 = unpack("C*", $s1);
		$a2 = unpack("C*", $s2);
		$diff = count($a1) ^ count($a2);
		$len = min(count($a1), count($a2));
		for ($i = 1; $i <= $len; $i++) {
			$diff |= $a1[$i] ^ $a2[$i];
		}
		return $diff === 0;
	}
	
	/**
	 * @param $username_or_email string
	 * @param PDO|null $db
	 * @return bool
	 */
	public static function userExists(string $username_or_email, ?PDO $db = null): bool {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT COUNT(*) FROM `users` WHERE LOWER(`email`)=LOWER(:username) OR "
			. "LOWER(`username`)=LOWER(:username)");
		$stmt->bindParam(":username", $username_or_email, PDO::PARAM_STR);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_COLUMN);
		return $results[0];
	}
	
	/**
	 * @param PDO|null $db
	 * @return array
	 */
	public static function listUsers(?PDO $db = null): array {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("SELECT `username`, `email` FROM `users` ORDER BY `id` ASC");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * @param int $length
	 * @param int $group_length
	 * @param string $group_separator
	 * @return string
	 * @throws Exception
	 */
	private static function generateNonce(int $length, int $group_length = 0, string $group_separator = "-"): string {
		$allowed = $_ENV["nonce_allowed_chars"];
		$nonce = "";
		for ($i = 0; $i < $length; $i++) {
			$index = random_int(0, strlen($allowed) - 1);
			$nonce .= substr($allowed, $index, 1);
		}
		if ($group_length > 0) {
			$nonce = chunk_split($nonce, $group_length, "-");
			$nonce = substr($nonce, 0, strlen($nonce) - 1);
		}
		
		return $nonce;
	}
	
	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}
	
	/**
	 * @return string
	 */
	public function getEmail(): string {
		return $this->email;
	}
	
	/**
	 * @return bool
	 */
	public function getEmailVerified(): bool {
		return $this->email_verified;
	}
	
	/**
	 * @return bool
	 */
	public function getAccountActive(): bool {
		return $this->active;
	}
	
	/**
	 * @param $new_password string
	 * @param PDO|null $db
	 * @throws Exception
	 */
	public function updatePassword(string $new_password, ?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		$salt = random_bytes(32);
		$new_password = static::hashPassword($new_password, $salt);
		$stmt =
			$db->prepare("UPDATE `users` SET `password_hash`=:password_hash, `password_salt`=:password_salt WHERE `id`=:id");
		$stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
		$stmt->bindParam(":password_hash", $new_password, PDO::PARAM_LOB);
		$stmt->bindParam(":password_salt", $salt, PDO::PARAM_LOB);
		$stmt->execute();
	}
	
	/**
	 * @param $new_email string
	 * @param PDO|null $db
	 * @throws EmailInUseException
	 */
	public function updateEmail(string $new_email, ?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		if (static::emailExists($new_email, $db)) {
			throw new EmailInUseException();
		}
		$stmt = $db->prepare("UPDATE `users` SET `email`=:email, `email_verified`=0 WHERE `id`=:id");
		$stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
		$stmt->bindParam(":email", $new_email, PDO::PARAM_STR);
		$stmt->execute();
		$this->email = $new_email;
		$this->email_verified = false;
	}
	
	public function sendEmailVerification(): void {
	
	}
	
	/**
	 * @param $new_username string
	 * @param PDO|null $db
	 * @throws UsernameInUseException
	 */
	public function updateUsername(string $new_username, ?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		if (static::usernameExists($new_username, $db)) {
			throw new UsernameInUseException();
		}
		$stmt = $db->prepare("UPDATE `users` SET `username`=:username WHERE `id`=:id");
		$stmt->bindParam(":username", $new_username, PDO::PARAM_STR);
		$stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
		try {
			$stmt->execute();
		}
		catch (PDOException $e) {
			if ($e->errorInfo[0] == 23000) {
				throw new UsernameInUseException();
			}
			throw $e;
		}
	}
	
	/**
	 * @throws PDOException
	 */
	public function deleteUser(?PDO $db = null): void {
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt = $db->prepare("DELETE FROM `users` WHERE `id`=:id");
		$stmt->bindParam(":id", $this->id, PDO::PARAM_STR);
		$stmt->execute();
	}
}

class UserException extends Exception {

}

class UsernameInUseException extends UserException {

}

class EmailInUseException extends UserException {

}

class InvalidCredentialsException extends UserException {

}
