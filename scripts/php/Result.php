<?php

class Result {
	public int $status;
	public bool $success;
	public mixed $data;
	public mixed $error;
	
	public function __construct(int $status = 200, bool $success = true, mixed $data = null, mixed $error = null) {
		$this->status = $status;
		$this->success = $success;
		$this->data = $data;
		if ($error !== null) {
			self::setFailed($status, $error);
		}
	}
	
	public function setFailed(int $status, $error = null): void {
		$this->status = $status;
		$this->success = false;
		if (is_string($error)) {
			$this->error = ["message" => $error];
		}
		else if ($error instanceof Throwable) {
			$this->error = [
				"class" => get_class($error),
				"message" => $error->getMessage()
			];
		}
		else {
			$this->error = $error;
		}
	}
	
	public function sendHttpResponse(bool $gzip = true): void {
		header("Content-type:application/json;charset=utf-8");
		http_response_code($this->status);
		if ($gzip) {
			ob_start("ob_gzhandler");
		}
		print(self::toJsonString());
		if ($gzip) {
			ob_end_flush();
		}
	}
	
	public function toJsonString(): bool|string {
		$filtered = array_filter((array)$this,
			function ($var) {
				return $var !== null;
			});
		return json_encode($filtered);
	}
}
