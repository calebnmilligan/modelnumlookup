<?php
include_once "User.php";
include_once "PermissionGroup.php";
include_once "Result.php";

$result = new Result();

try {
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		if (!isset($_GET["uid"])) {
			$result->setFailed(400, "Missing parameter \"uid\"");
		}
		else if (!isset($_GET["permission"])) {
			$result->setFailed(400, "Missing parameter \"permission\"");
		}
		else {
			$user_id = $_GET["uid"];
			$permission = $_GET["permission"];
			$user = User::loadUser($user_id);
			$value = PermissionGroup::anyGroupHasPermission($user->permission_groups, $permission);
			$result->data = [
				"user_id" => $user_id,
				"permission" => $permission,
				"value" => $value
			];
		}
	}
	else {
		$result->setFailed(405, "Invalid method (Expected GET)");
		header("Allow:GET");
	}
}
catch (Exception $e) {
	$result->setFailed($e instanceof UserException ? 400 : 500, $e);
}
$result->sendHttpResponse();
