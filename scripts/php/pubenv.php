<?php
include_once "env.php";

$_PUBENV = [];
$_PUBENV["session_expire_minutes"] = 30;
$_PUBENV["password_length_min"] = 8;
$_PUBENV["nonce_allowed_chars"] = "abcdefghjkmnpqrstuvwxyz0123456789";

foreach ($_PUBENV as $key => $value) {
	$_ENV[$key] = $value;
}
