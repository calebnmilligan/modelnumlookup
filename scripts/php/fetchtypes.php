<?php
include_once "conn.php";
include_once "Result.php";

$result = new Result();

try {
	$conn = getDBConnection();
	$stmt = $conn->prepare("SELECT DISTINCT(`type`) FROM `model_lookup` WHERE `type` IS NOT NULL ORDER BY `type`");
	$stmt->execute();
	$result->data = $stmt->fetchAll(PDO::FETCH_COLUMN);
}
catch (Exception $e) {
	$result->setFailed(500, $e);
	error_log($e);
}
$result->sendHttpResponse();
