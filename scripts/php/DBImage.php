<?php
include_once "conn.php";
include_once "UUID.php";

class DBImage {
	private UUID $id;
	private string $name;
	private ?string $uploader;
	private ?string $description;
	private string $mime_type;
	private ?string $image;
	
	/**
	 * @param UUID|string $id
	 * @param string $name
	 * @param string|null $uploader
	 * @param string|null $description
	 * @param string $mime_type
	 * @param string|null $image
	 */
	private function __construct(UUID|string $id, string $name, ?string $uploader, ?string $description,
								 string      $mime_type, string $image = null) {
		$this->id = new UUID($id);
		$this->name = $name;
		$this->uploader = $uploader;
		$this->description = $description;
		$this->mime_type = $mime_type;
		$this->image = $image;
	}
	
	/**
	 * @param string $name
	 * @param string|null $uploader
	 * @param string|null $description
	 * @param string $mime_type
	 * @param string $image_file
	 * @param PDO|null $db
	 * @return DBImage|null
	 * @throws ImagickException
	 */
	public static function storeImage(string $name, ?string $uploader, ?string $description, string $mime_type,
									  string $image_file, ?PDO $db = null): ?DBImage {
		self::stripExifData($image_file);
		$image = file_get_contents($image_file);
		$tl = file_get_contents(self::resizeImage($image_file, 600, "large"));
		$tm = file_get_contents(self::resizeImage($image_file, 300, "medium"));
		$ts = file_get_contents(self::resizeImage($image_file, 150, "small"));
		$id = UUID::genUuidV4("images");
		if ($db == null) {
			$db = getDBConnection();
		}
		$stmt =
			$db->prepare("INSERT INTO `images` (`id`, `name`, `uploader`, `description`, `mime_type`, `image`, "
				. "`thumb_large`, `thumb_medium`, `thumb_small`) VALUES (:id, :name, :uploader, :description, "
				. ":mime_type, :image, :tl, :tm, :ts)");
		$stmt->bindParam(":id", $id, PDO::PARAM_STR);
		$stmt->bindParam(":name", $name, PDO::PARAM_STR);
		$stmt->bindParam(":uploader", $uploader, PDO::PARAM_STR);
		$stmt->bindParam(":description", $description, PDO::PARAM_STR);
		$stmt->bindParam(":mime_type", $mime_type, PDO::PARAM_STR);
		$stmt->bindParam(":image", $image, PDO::PARAM_STR);
		$stmt->bindParam(":tl", $tl, PDO::PARAM_STR);
		$stmt->bindParam(":tm", $tm, PDO::PARAM_STR);
		$stmt->bindParam(":ts", $ts, PDO::PARAM_STR);
		if ($stmt->execute()) {
			return new DBImage($id, $name, $uploader, $description, $mime_type, null);
		}
		return null;
	}
	
	/**
	 * @param string $file
	 * @throws ImagickException
	 */
	public static function stripExifData(string $file) {
		$imagick = new Imagick($file);
		static $whitelisted_tags = [
			"jpeg:.*",
			"exif:Orientation",
			"exif:ColorSpace",
			"exif:WhiteBalance"
		];
		$properties = $imagick->getImageProperties("*", true);
		foreach ($properties as $key => $_) {
			$remove = true;
			for ($j = 0; $j < sizeof($whitelisted_tags); $j++) {
				if (preg_match("/$whitelisted_tags[$j]/iS", $key)) {
					$remove = false;
					break;
				}
			}
			if ($remove) {
				$imagick->deleteImageProperty($key);
			}
		}
		$imagick->writeImage($file);
		$imagick->clear();
		$imagick->destroy();
	}
	
	/**
	 * @param string $file
	 * @param int $maxwidth
	 * @param string $desc
	 * @param string|null $image
	 * @return string
	 * @throws ImagickException
	 */
	public static function resizeImage(string $file, int $maxwidth, string $desc, string $image = null): string {
		$imagick = new Imagick($file);
		$size = $imagick->getImageGeometry();
		$ratio = $size["width"] / $size["height"];
		$width = $maxwidth;
		$height = $maxwidth / $ratio;
		$imagick->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 0.0);
		$name = pathinfo($file, PATHINFO_FILENAME);
		$target_file = $name . "_" . $desc . ".jpg";
		$tempfile = tempnam(sys_get_temp_dir(), $target_file);
		$imagick->writeImage($tempfile);
		$imagick->clear();
		$imagick->destroy();
		return $tempfile;
	}
	
	/**
	 * @param UUID|string $id
	 * @param int|null $thumb_size
	 * @param PDO|null $db
	 * @return DBImage|null
	 */
	public static function fetchImage(UUID|string $id, int $thumb_size = null, ?PDO $db = null): ?DBImage {
		$id = (new UUID($id))->getStringRep();
		if ($db == null) {
			$db = getDBConnection();
		}
		if ($thumb_size === null) {
			$thumb_size = 0;
		}
		$thumb_size_name = $thumb_size == 1 ? "large" : ($thumb_size == 2 ? "medium" : "small");
		$stmt = $thumb_size <= 0
			?
			$db->prepare("SELECT `name`, `uploader`, `description`, `mime_type`, `image` FROM `images` WHERE `id`=:id")
			:
			$db->prepare("SELECT `name`, `uploader`, `description`, `thumb_$thumb_size_name` AS `image` FROM `images` WHERE `id`=:id");
		$stmt->bindParam(":id", $id, PDO::PARAM_STR);
		if (!$stmt->execute()) {
			return null;
		}
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if (!$result) {
			return null;
		}
		$name = $result["name"];
		if ($thumb_size > 0) {
			$name = pathinfo($name, PATHINFO_FILENAME) . "_" . $thumb_size_name . ".jpg";
		}
		return new DBImage(
			$id,
			$name,
			$result["uploader"],
			$result["description"],
			$thumb_size <= 0 ? $result["mime_type"] : "image/jpeg",
			$result["image"]
		);
	}
	
	/**
	 * @return UUID
	 */
	public function getId(): UUID {
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @return string|null
	 */
	public function getUploader(): ?string {
		return $this->uploader;
	}
	
	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @return string
	 */
	public function getMimeType(): string {
		return $this->mime_type;
	}
	
	/**
	 * @return bool
	 */
	public function hasImage(): bool {
		return $this->image !== null;
	}
	
	/**
	 * @return string|null
	 */
	public function getImage(): ?string {
		return $this->image;
	}
}
