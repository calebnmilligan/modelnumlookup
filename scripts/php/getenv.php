<?php
include_once "pubenv.php";
include_once "Result.php";

$result = new Result();
$result->data = [];

if ($_SERVER["REQUEST_METHOD"] === "GET") {
	if (!isset($_GET["key"])) {
		$result->setFailed(400, "Missing parameter \"key\"");
	}
	else {
		$keys = $_GET["key"];
		if (!is_array($keys)) {
			$keys = explode(",", $keys);
		}
		foreach ($keys as $key) {
			$key = urldecode(trim($key));
			$result->data[$key] = $_PUBENV[$key] ?? null;
		}
	}
}
else {
	$result->setFailed(405);
	header("Allow:GET");
}
$result->sendHttpResponse();
