<?php
include_once "User.php";
include_once "Session.php";
include_once "pubenv.php";
include_once "Result.php";

$result = new Result();

try {
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!isset($_POST["uname"])) {
			$result->setFailed(400, "Missing parameter \"uname\"");
		}
		else if (!isset($_POST["passwd"])) {
			$result->setFailed(400, "Missing parameter \"passwd\"");
		}
		else {
			$username = $_POST["uname"];
			$password = $_POST["passwd"];
			$user = User::loginUser($username, $password);
			$session = Session::createSession($user->id);
			$result->data = [
				"user" => $user,
				"session" => $session
			];
			setcookie("session_id", $session->id, time() + $_ENV["session_expire_minutes"] * 60, "/");
		}
	}
	else if ($_SERVER["REQUEST_METHOD"] == "GET") {
		if (!isset($_GET["sid"])) {
			$result->setFailed(400, "Missing parameter \"sid\"");
		}
		else {
			$session_id = $_GET["sid"];
			Session::extendSession($session_id);
			$session = Session::getSession($session_id);
			if ($session === null) {
				$result->setFailed(400, "Invalid session");
			}
			else {
				$result->data = $session;
			}
		}
	}
	else {
		$result->setFailed(405, "Invalid method (Expected GET or POST)");
		header("Allow:GET,POST");
	}
}
catch (Exception $e) {
	if ($e instanceof InvalidCredentialsException) {
		$result->setFailed(401, "Invalid credentials");
	}
	else {
		$result->setFailed(500, $e);
	}
}
$result->sendHttpResponse();
