<?php
include_once "conn.php";

class UUID {
	private string $repr;
	
	public function __construct(UUID|string $repr) {
		if ($repr instanceof UUID) {
			$this->repr = $repr->repr;
		}
		else {
			if (!preg_match("/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/i", $repr)) {
				throw new InvalidArgumentException("Not a UUID");
			}
			$this->repr = strtolower($repr);
		}
	}
	
	public static function genUuidV4(string $table = null, ?PDO $db = null): ?UUID {
		if ($db == null) {
			$db = getDBConnection();
		}
		for ($i = 0; $i < 16; $i++) {
			$data = openssl_random_pseudo_bytes(16);
			$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
			$data[8] = chr(ord($data[8]) & 0x3f | 0x80);
			$uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
			if (!UUID::uuidExists($uuid, $table, $db)) {
				return new UUID($uuid);
			}
		}
		return null;
	}
	
	public static function uuidExists(string $uuid, string $table = null, ?PDO $db = null) {
		if ($db == null) {
			$db = getDBConnection();
		}
		$has_table = $table !== null;
		$extra = $has_table ? " AND `source_table`=:table" : "";
		$query = "SELECT EXISTS(SELECT * FROM `uuids` WHERE `id`=:uuid$extra)";
		$stmt = $db->prepare($query);
		if ($has_table) {
			$stmt->bindParam(":table", $table, PDO::PARAM_STR);
		}
		$stmt->bindParam(":uuid", $uuid, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_COLUMN);
		return $result;
	}
	
	public function __toString() {
		return self::getStringRep();
	}
	
	public function getStringRep(): string {
		return $this->repr;
	}
}
