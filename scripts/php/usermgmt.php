<?php
include_once "User.php";
include_once "PermissionGroup.php";
include_once "Result.php";

$result = new Result();
try {
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!isset($_POST["uname"])) {
			$result->setFailed(400, "Missing parameter \"uname\"");
		}
		else if (!isset($_POST["email"])) {
			$result->setFailed(400, "Missing parameter \"email\"");
		}
		else if (!isset($_POST["passwd"])) {
			$result->setFailed(400, "Missing parameter \"passwd\"");
		}
		else {
			$username = $_POST["uname"];
			$email = $_POST["email"];
			$password = $_POST["passwd"];
			$user = User::createUser($username, $email, $password);
			$result->data = $user;
		}
	}
	else if ($_SERVER["REQUEST_METHOD"] == "GET") {
		if (isset($_GET["id"])) {
			$user_id = $_GET["id"];
			$result->data = User::loadUser($user_id);
		}
		else if (isset($_GET["uname"])) {
			$username = $_GET["uname"];
			$result->data = User::usernameExists($username);
		}
		else if (isset($_GET["email"])) {
			$email = $_GET["email"];
			$result->data = User::emailExists($email);
		}
		else {
			$result->setFailed(400, "Missing parameter \"uname\" or \"email\"");
		}
	}
	else {
		$result->setFailed(405, "Invalid method (Expected GET, POST, or DELETE)");
		header("Allow:GET,POST,DELETE");
	}
}
catch (Exception $e) {
	$result->setFailed($e instanceof UserException ? 400 : 500, $e);
}
$result->sendHttpResponse();
