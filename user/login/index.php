<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Sign In</title>
		<link href="../../styles/theme.css" rel="stylesheet" type="text/css">
		<link href="../../styles/login.css" rel="stylesheet" type="text/css">
		<script src="../../scripts/js/utils.js" type="application/javascript"></script>
		<script src="../../scripts/js/session.js" type="application/javascript"></script>
		<script src="../../scripts/js/login.js" type="application/javascript"></script>
	</head>
	<body>
		<form accept-charset="UTF-8" id="form-login">
			<div>
				<label for="input-username">Username</label>
				<input autocomplete="username" id="input-username" type="text">
			</div>
			<div>
				<label for="input-password">Password</label>
				<input autocomplete="current-password" id="input-password" type="password">
			</div>
			<div>
				<input type="submit">
			</div>
		</form>
		<input type="hidden" id="referrer" value="<?php print($_SERVER['HTTP_REFERER'] ?? ''); ?>">
	</body>
</html>