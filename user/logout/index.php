<?php
include_once "../../scripts/php/Session.php";
if (isset($_COOKIE["session_id"])) {
	try {
		$session_id = new UUID($_COOKIE["session_id"]);
		Session::endSession($session_id);
		setcookie("session_id", "", time() - 3600, "/");
	}
	catch (InvalidArgumentException $e) {
		// Do noting
	}
}
$ref = $_SERVER["HTTP_REFERER"] ?? "../../";
header("Location:$ref");
