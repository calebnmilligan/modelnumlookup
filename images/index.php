<?php
include_once "../scripts/php/conn.php";

$db = getDBConnection();
$stmt = $db->prepare("SELECT `i`.`id`, `i`.`name`, `i`.`mime_type` FROM `images` AS `i` LEFT JOIN "
	. "`model_lookup_images` AS `mli` ON `i`.`id`=`mli`.`image_id` ORDER BY `mli`.`model_id`, "
	. "`mli`.`order`, `i`.`id`;");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en_US">
	<head>
		<meta charset="UTF-8">
		<meta content="Database of products and suggested pricing" name="description">
		<meta content="Caleb Milligan" name="author">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<title>Product Database Lookup</title>
		<script type="application/javascript">
			function sendDelete(arg) {
				let request = new XMLHttpRequest();
				request.open("DELETE", "../scripts/php/image.php?id=" + arg);
				request.onreadystatechange = function () {
					if (this.readyState === 4) {
						let response = JSON.parse(this.responseText);
						console.log(response);
					}
				};
				request.send();
			}
		</script>
		<style>
			td {
				border: 1px solid black;
			}
			
			img {
				max-width: 400px;
				max-height: 400px;
				width: auto;
				height: auto;
			}
			
			a {
				text-decoration: none;
			}
			
			* {
				font-family: "Consolas", monospace;
			}
		</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th>Large Thumbnail</th>
					<th>Medium Thumbnail</th>
					<th>Small Thumbnail</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($result as $row) {
					?>
					<tr>
						<td colspan="4">
							<a target="_blank" href="../scripts/php/image.php?id=<?php print($row["id"]) ?>">
								<?php printf("%s (%s): %s", $row["name"], $row["id"], $row["mime_type"]) ?>
							</a>
						</td>
					</tr>
					<tr>
						<td>
							<a target="_blank"
							   href="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=large">
								<img src="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=large"/>
							</a>
						</td>
						<td>
							<a target="_blank"
							   href="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=medium">
								<img src="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=medium"/>
							</a>
						</td>
						<td>
							<a target="_blank"
							   href="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=small">
								<img src="../scripts/php/image.php?id=<?php print($row["id"]) ?>&thumb=small"/>
							</a>
						</td>
						<td><input type="button" value="Delete" onclick="sendDelete('<?php print($row["id"]) ?>')"></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</body>
</html>
