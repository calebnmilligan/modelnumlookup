DROP VIEW IF EXISTS `uuids`;
DROP VIEW IF EXISTS `model_lookup_master`;
DROP VIEW IF EXISTS `model_lookup_prices`;
DROP TABLE IF EXISTS `model_lookup_images`;
DROP TABLE IF EXISTS `images`;
DROP TABLE IF EXISTS `model_lookup`;
DROP TABLE IF EXISTS `permission_group_permissions`;
DROP TABLE IF EXISTS `permission_group_users`;
DROP TABLE IF EXISTS `permission_groups`;
DROP TABLE IF EXISTS `sessions`;
DROP TABLE IF EXISTS `user_email_verification`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `idempotency`;

CREATE TABLE `images`
(
	`id`           CHARACTER(36) NOT NULL,
	`name`         TINYTEXT      NOT NULL,
	`uploader`     CHARACTER(36) DEFAULT NULL,
	`description`  TEXT          DEFAULT NULL,
	`mime_type`    TINYTEXT      NOT NULL,
	`image`        MEDIUMBLOB    NOT NULL,
	`thumb_large`  MEDIUMBLOB    NOT NULL,
	`thumb_medium` MEDIUMBLOB    NOT NULL,
	`thumb_small`  MEDIUMBLOB    NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `model_lookup`
(
	`id`           INTEGER        NOT NULL AUTO_INCREMENT,
	`model_number` TINYTEXT DEFAULT NULL,
	`make`         TINYTEXT DEFAULT NULL,
	`model`        TINYTEXT       NOT NULL,
	`type`         TINYTEXT       NOT NULL,
	`base_price`   DECIMAL(13, 4) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `model_lookup_images`
(
	`model_id` INTEGER       NOT NULL,
	`image_id` CHARACTER(36) NOT NULL,
	`order`    INTEGER       NOT NULL DEFAULT 0,
	UNIQUE KEY (`model_id`, `image_id`),
	FOREIGN KEY (`model_id`) REFERENCES `model_lookup` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `users`
(
	`id`             CHARACTER(36) NOT NULL,
	`username`       TINYTEXT      NOT NULL,
	`email`          TEXT          NOT NULL,
	`password_hash`  BINARY(64),
	`password_salt`  BINARY(32),
	`email_verified` BOOLEAN       NOT NULL DEFAULT FALSE,
	`active`         BOOLEAN       NOT NULL DEFAULT FALSE,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`username`),
	UNIQUE KEY (`email`)
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `user_email_verification`
(
	`user_id` CHARACTER(36) NOT NULL,
	`nonce`   CHARACTER(6)  NOT NULL,
	PRIMARY KEY (`user_id`),
	UNIQUE KEY (`nonce`)
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `permission_groups`
(
	`id`     INTEGER  NOT NULL AUTO_INCREMENT,
	`name`   TINYTEXT NOT NULL,
	`parent_id` INTEGER,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`name`),
	FOREIGN KEY (`parent_id`) REFERENCES `permission_groups` (`id`) ON DELETE SET NULL
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `permission_group_users`
(
	`group_id` INTEGER       NOT NULL,
	`user_id`  CHARACTER(36) NOT NULL,
	FOREIGN KEY (`group_id`) REFERENCES `permission_groups` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `permission_group_permissions`
(
	`group_id`        INTEGER NOT NULL,
	`permission_name` TEXT    NOT NULL,
	`value`           BOOLEAN NOT NULL,
	FOREIGN KEY (`group_id`) REFERENCES `permission_groups` (`id`) ON DELETE CASCADE
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `sessions`
(
	`id`         CHARACTER(36) NOT NULL,
	`user_id`    CHARACTER(36) NOT NULL,
	`started_at` TIMESTAMP     NOT NULL,
	`expires_at` DATETIME      NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE TABLE `idempotency`
(
	`id`          CHARACTER(36) NOT NULL,
	`timestamp`   TIMESTAMP     NOT NULL,
	`status_code` SMALLINT      NOT NULL,
	`headers`     TEXT          NOT NULL,
	`response`    MEDIUMTEXT    NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE InnoDB
  CHARSET `utf8mb4`
  COLLATE `utf8mb4_unicode_520_ci`;

CREATE VIEW `uuids`
AS
SELECT `id`     AS `id`,
	   "images" AS `source_table`
FROM `images`
UNION
SELECT `id`    AS `id`,
	   "users" AS `source_table`
FROM `users`
UNION
SELECT `id`       AS `id`,
	   "sessions" AS `source_table`
FROM `sessions`
UNION
SELECT `id`          AS `id`,
	   "idempotency" AS `source_table`
FROM `idempotency`;

CREATE VIEW `model_lookup_prices`
AS
SELECT `model_lookup`.`id`                                        AS `model_id`,
	   `model_lookup`.`base_price`                                AS `base_price`,
	   GREATEST(FLOOR(`model_lookup`.`base_price`), 1.00)         AS `condition_excellent`,
	   GREATEST(FLOOR(`model_lookup`.`base_price` * 0.875), 1.00) AS `condition_very_good`,
	   GREATEST(FLOOR(`model_lookup`.`base_price` * 0.75), 1.00)  AS `condition_good`,
	   GREATEST(FLOOR(`model_lookup`.`base_price` * 0.625), 1.00) AS `condition_fair`,
	   GREATEST(FLOOR(`model_lookup`.`base_price` * 0.5), 1.00)   AS `condition_poor_untested`,
	   GREATEST(FLOOR(`model_lookup`.`base_price` * 0.25), 1.00)  AS `condition_nonfunctional`
FROM `model_lookup`
WHERE `model_lookup`.`base_price` IS NOT NULL;

CREATE VIEW `model_lookup_master`
AS
SELECT `model_lookup`.`id`                             AS `id`,
	   `model_lookup`.`model_number`                   AS `model_number`,
	   `model_lookup`.`make`                           AS `make`,
	   `model_lookup`.`model`                          AS `model`,
	   `model_lookup`.`type`                           AS `type`,
	   `model_lookup`.`base_price`                     AS `base_price`,
	   `model_lookup_prices`.`condition_excellent`     AS `condition_excellent`,
	   `model_lookup_prices`.`condition_very_good`     AS `condition_very_good`,
	   `model_lookup_prices`.`condition_good`          AS `condition_good`,
	   `model_lookup_prices`.`condition_fair`          AS `condition_fair`,
	   `model_lookup_prices`.`condition_poor_untested` AS `condition_poor_untested`,
	   `model_lookup_prices`.`condition_nonfunctional` AS `condition_nonfunctional`
FROM (`model_lookup`
		 LEFT JOIN `model_lookup_prices`
				   ON (`model_lookup_prices`.`model_id` = `model_lookup`.`id`));
